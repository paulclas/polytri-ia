# Poly-TRI IA

Poly-TRI IA is an open-source project that aims to develop a simple and customizable trash classification model to be installed on a Raspeberry-Pi and Pi Camera to conduct object detection and classification tasks.

* Ghali Chraibi
* Esther Guerrier
* Olivier Saint-Cyr
* Paul Clas

## Installation
Clone the repository by executing the following command :
1. `https://gitlab.com/paulclas/polytri-ia.git`
2. `git checkout RR`

The React client listens on port:  [http://localhost:3000](http://localhost:3000)

The main server listens on port: [http://localhost:5000](http://localhost:5000)

## Usage
Clone the repository by executing the following command :
1. `git clone https://gitlab.com/paulclas/polytri-ia.git`

### Dependancies
1. Docker-compose 1.25.0 
2. Tensorflow
3. Yolo
4. Debian Raspeberry OS
5. Python3

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)

