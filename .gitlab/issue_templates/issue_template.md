## Description de la tâche à réaliser

Votre description ici.

## Découpage en sous-tâches
1. [x] Tâche 1
2. [ ] Tâche 2
   1. [ ] Sous-tâche 1
   2. [x] Sous-tâche 2 

## Comment vérifier la validité de la solution qui sera proposée (test, mesure ... ) ?

Remplir ici.

## Comment reproduire (dans le cas d' un rapport de bogue) ?

Remplir ici

## Requis traité(s)

Remplir ici.


## Autre (source, documentation, remarque, avis ...) : 

Remplir ici 
